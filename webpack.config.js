var path = require("path");

module.exports = {
  entry: {
    app: ["./index.js"]
  },
  devServer: {
    inline: true,
	historyApiFallback: true,
    stats: 'errors-only'
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    publicPath: "/dist/",
    filename: "bundle.js"
  }
};